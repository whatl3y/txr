import memory from './memory'
import redis from './redis'

export default {
  memory,
  redis
}
